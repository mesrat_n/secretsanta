(function() {
  const myQuestions = [
    {
      question: "The key is composite of what you don't see until you take it to the edge.",
      hint: {},
      correctAnswer: "3264"
    },
    {
      question: "The more you take, the more you leave behind.",
      hint: {},
      correctAnswer: "footsteps"
    },
    {
      question: "Fill out missing letters to complete the word.",
      hint: "Q....",
      correctAnswer: "queue"
    },
    {
      question: "It goes like this: four letters, six rows, only one answer.",
      hint: "C O N S S M O N S N O M M S E O C O M E I T O N",
      correctAnswer: "Voltaire"
    },
    {
      question: "Think out of the box to open the box. Code to combination lock is inside.",
      hint: "<img style='height: 10em;' src='./box.png'>",
      correctAnswer: "aha!"
    }
  ];

  function buildQuiz() {
    const output = [];

    myQuestions.forEach((currentQuestion, questionNumber) => {
      const hints = [];

      if (Object.entries(currentQuestion.hint).length !== 0) {
          hints.push(
              `<p>${currentQuestion.hint}</p>
              `
          );
      }

      if (questionNumber == 4) {
        output.push(
          `<div id="slide" class="slide">
              <div class="question"> ${currentQuestion.question} </div>
              <div class="hints"> ${hints.join("")} </div>
          </div>`
        );
      } else {
        output.push(
          `<div id="slide" class="slide">
              <div class="question"> ${currentQuestion.question} </div>
              <div class="hints"> ${hints.join("")} </div>
              <div class="answers">
                  <input type="text" id="question${questionNumber}" name="question${questionNumber}" required>
              </div>
          </div>`
        );
      }

    });

    quizContainer.innerHTML = output.join("");
  }

  function checkAnswer(n) {
    const id = "question"+n;
    const userAnswer = document.getElementById(id).value;
    const rightAnswer = myQuestions[n].correctAnswer;
    if (userAnswer === rightAnswer) {
      return true;
    } else {
      return false;
    }
  }

  function showSlide(n) {
    slides[currentSlide].classList.remove("active-slide");
    slides[n].classList.add("active-slide");
    currentSlide = n;
    
    if (currentSlide === 0) {
      previousButton.style.display = "none";
    } else {
      previousButton.style.display = "inline-block";
    }
    
    if (currentSlide === slides.length - 1) {
      nextButton.style.display = "none";
    } else {
      nextButton.style.display = "inline-block";
    }
  }
  
  function addElement (text, id) {
    var newContent = document.createElement("P");
    var t = document.createTextNode(text);
    newContent.appendChild(t);
    newContent.style.color = "red";
    newContent.setAttribute("id", id);
    var slideDiv = document.getElementById("slide");
    slideDiv.appendChild(newContent);
  }

  function showNextSlide() {
    if (checkAnswer(currentSlide)) {
      var element = document.getElementById("falseAnswer");
      if (element) {
        element.parentNode.removeChild(element);   
      }
      showSlide(currentSlide + 1);
    } else {
      addElement("Wrong answer! Try again!", "falseAnswer");
    }
  }

  function showPreviousSlide() {
    showSlide(currentSlide - 1);
  }

  const quizContainer = document.getElementById("quiz");

  buildQuiz();

  const previousButton = document.getElementById("previous");
  const nextButton = document.getElementById("next");
  const slides = document.querySelectorAll(".slide");
  let currentSlide = 0;

  showSlide(0);

  previousButton.addEventListener("click", showPreviousSlide);
  nextButton.addEventListener("click", showNextSlide);
})();
